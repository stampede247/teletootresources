//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform vec2 TextureSize;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fSampleCoord;

void main()
{
	fColor = inColor;
	fSampleCoord = inTexCoord / TextureSize;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;

in vec4 fColor;
in vec2 fSampleCoord;

out vec4 frag_colour;

void main()
{
	frag_colour = fColor * texture(Texture, fSampleCoord);
}