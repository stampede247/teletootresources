//======================== VERTEX_SHADER ========================

#version 130

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = (SourceRectangle.xy + (inTexCoord * SourceRectangle.zw)) / TextureSize;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 130

uniform sampler2D Texture;
uniform vec2 TextureSize;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;

uniform bool      GradientEnabled;
uniform float     CircleRadius;
uniform float     CircleInnerRadius;
uniform float     Saturation;
uniform float     Brightness;

uniform float Value0; //vignetteInnerRadius
uniform float Value1; //vignetteOuterRadius

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;

out vec4 frag_colour;

vec3 czm_saturation(vec3 rgb, float adjustment)
{
	// Algorithm from Chapter 16 of OpenGL Shading Language
	const vec3 W = vec3(0.2125, 0.7154, 0.0721);
	vec3 intensity = vec3(dot(rgb, W));
	return mix(intensity, rgb, adjustment);
}

vec3 srgb_to_linear(vec3 color) {
	// note: some people use an approximation for the gamma of 2.0, for efficiency, but 2.2 is more correct
	return vec3(pow(color.r, 2.2), pow(color.g, 2.2), pow(color.b, 2.2));
}

vec3 linear_to_srgb(vec3 color) {
	// note: if using gamma of 2.0, instead can use 0.5 as the value here
	return vec3(pow(color.r, 1.0/2.2), pow(color.g, 1.0/2.2), pow(color.b, 1.0/2.2));
}

void main()
{
	vec4 sampleColor = texture(Texture, fSampleCoord);
	
	sampleColor = fColor * sampleColor;
	
	if (GradientEnabled)
	{
		float gray = dot(sampleColor.rgb, vec3(0.299, 0.587, 0.114));
		frag_colour = mix(PrimaryColor.rgba, SecondaryColor.rgba, gray);
	}
	else
	{
		frag_colour = PrimaryColor * sampleColor;
	}
	
	if (CircleRadius != 0)
	{
		float distFromCenter = length(fTexCoord - vec2(0.5, 0.5)) * 2;
		float smoothDelta = fwidth(distFromCenter);
		frag_colour.a *= smoothstep(CircleRadius, CircleRadius-smoothDelta, distFromCenter);
		if (CircleInnerRadius != 0)
		{
			frag_colour.a *= smoothstep(CircleInnerRadius, CircleInnerRadius+smoothDelta, distFromCenter);
		}
	}
	
	if (Value1 > 0)
	{
		float distFromCenter = length(fTexCoord - vec2(0.5, 0.5))*2;
		if (distFromCenter > Value1)
		{
			frag_colour.rgb = vec3(0);
		}
		else if (distFromCenter > Value0)
		{
			frag_colour.rgb = srgb_to_linear(frag_colour.rgb);
			frag_colour.rgb = mix(frag_colour.rgb, vec3(0), (distFromCenter - Value0) / (Value1 - Value0));
			frag_colour.rgb = linear_to_srgb(frag_colour.rgb);
		}
	}
	
	frag_colour.rgb = czm_saturation(frag_colour.rgb, Saturation);
	frag_colour.rgb *= Brightness;
	
	if (frag_colour.a < 0.1 && !GradientEnabled) { discard; }
}